package com.example.gm.dbrecyclerview;

import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.widget.ImageView;

public class Customsetter {

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @BindingAdapter("imgSrc")
    public static void setImgSrc(ImageView imageView, int resId) {
        imageView.setImageDrawable(imageView.getContext().getDrawable(resId));
    }
}

