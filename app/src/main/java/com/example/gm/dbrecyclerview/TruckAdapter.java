package com.example.gm.dbrecyclerview;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.gm.dbrecyclerview.databinding.ItemsBinding;

import java.util.ArrayList;

public class TruckAdapter extends RecyclerView.Adapter<TruckAdapter.ViewHolder> {
    private Context context;
    private ArrayList<TruckList> tList;
    private LayoutInflater inflater;

    public TruckAdapter(Context context, ArrayList<TruckList> tList) {
        this.context = context;
        this.tList = tList;
    }

    @NonNull
    @Override
    public TruckAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        inflater=LayoutInflater.from(viewGroup.getContext());
        ItemsBinding dataBinding=ItemsBinding.inflate(inflater,viewGroup,false);


        return new ViewHolder(dataBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull TruckAdapter.ViewHolder viewHolder, int i) {

        TruckList truckList=tList.get(i);
        viewHolder.mDataBinding.setTruckList(truckList);

    }

    @Override
    public int getItemCount() {
        return tList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ItemsBinding mDataBinding;
        public ViewHolder(ItemsBinding dataBinding) {
            super(dataBinding.getRoot());
            this.mDataBinding=dataBinding;
        }


    }


}
